﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnderGroundChinchiro
{
    public enum RollType { SnakeEyes = 5, Triples =3, FourFiveSix =2, Pair = 1, OneTwoThree = -2,  Bust = 0, Pisser =-1}
    class RollEvaluator
    {
        public static RollType EvaluateRoll(Roll roll)
        {
            Array.Sort(roll.Values);

            if (roll.Values[0] == 1 && roll.Values[1] == 1 && roll.Values[2] == 1)
            {
                return RollType.SnakeEyes;
            }
            if (roll.Values[0] == roll.Values[1] && roll.Values[1] == roll.Values[2])
            {
                return RollType.Triples;
            }
            if(roll.Values[0] == 4 && roll.Values[1] == 5 && roll.Values[2] == 6)
            {
                return RollType.FourFiveSix;
            }
            if((roll.Values[0] == roll.Values[1])||(roll.Values[1] == roll.Values[2]))
            {
                return RollType.Pair;
            }
            if(roll.Values[0] == 1 && roll.Values[1] == 2 && roll.Values[2] == 3 )
            {
                return RollType.OneTwoThree;
            }

            return RollType.Bust;
        }
    }
}
