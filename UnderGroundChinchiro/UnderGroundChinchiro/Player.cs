﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnderGroundChinchiro
{
    class Player
    {
        public Player()
        {
            Name = "default";
            Dices = new Roll();
            Money = 90000;
            Bet = 0;
        }
        public Player(string name, int startingMoney)
        {
            Name = name;
            Dices = new Roll();
            Money = startingMoney;
            Bet = 0;
        }
      
        public string Name;
        public int Bet;
        public int Money;
        public Roll Dices;
       
        
    }
}
