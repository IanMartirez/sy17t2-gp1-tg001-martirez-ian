﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnderGroundChinchiro
{
    class Program
    {

        static void Main(string[] args)
        {
            Player Player = new Player("Kaiji", 90000);
            Player Dealer = new Player("Ohtsuki (Dealer)", 1000);

            for (int i = 1; i <= 10; i++)
            {
                Console.WriteLine("ROUND: " + i.ToString() + " of 10");
                Console.WriteLine();
                if (CanBet(Player.Money, 100))
                {
                    Player.Bet = Wager(Player.Money);
                }
                else
                {
                    Console.WriteLine(Player.Name + " Money: " + Player.Money);
                    Console.WriteLine("Sorry you cannot bet anymore");
                    Console.WriteLine("Press Any Key to Continue...");
                    Console.ReadKey();
                    Console.WriteLine();
                    break;
                }

                Console.WriteLine("ROUND: " + i.ToString() + " of 10");
                Console.WriteLine();
                Turn(Dealer, 5);

                Console.Clear();

                if (Dealer.Dices.RollType == RollType.Pisser)
                {
                    Console.WriteLine(Dealer.Name + " automatically loose...");
                    Player.Money += Player.Bet;
                    Console.WriteLine(Player.Name + " wins: " + Player.Bet + " Perica");
                    Console.Write("Press Any Key to Continue...");
                    Console.ReadKey();
                    Console.Clear();
                }
                else
                {
                    Console.WriteLine("ROUND: " + i.ToString() + " of 10");
                    Console.WriteLine();
                    Turn(Player, 5);

                    Console.Clear();
                    EvaluateRound(Dealer, Player);
                }
            }

            EvaluateEnding(Player.Money);
        }

        static void Turn(Player p, int chanceToPisser)
        {
            Console.WriteLine(p.Name + " Roll: ");
            Console.WriteLine();
            RollUntilValid(p, chanceToPisser);
        }
        static int Wager(int money)
        {
            int bet = 0;
            do
            {
                Console.WriteLine("Perica: " + money);
                Console.WriteLine("Minimun Bet: " + 100);
                Console.Write("Input Bet: ");
                bet = Convert.ToInt32(Console.ReadLine());
                Console.WriteLine();
                Console.Clear();
            } while (bet < 100 || bet > money);
            return bet;
        }
        static void PrintRoll(Roll roll)
        {
            if (roll.RollType == RollType.Pisser)
            {
                Console.WriteLine("Pisser");
            }
            else
            {
                Console.Write(roll.RollType);
                Console.Write(": {");
                foreach (int number in roll.Values)
                {
                    Console.Write(number);
                }
                Console.Write("}");
                if (roll.RollType == RollType.Pair)
                {
                    Console.Write(" (" + RollEvaluatePair(roll.Values) + ")");
                }
                Console.WriteLine();
            }


        }
        static void EvaluateRound(Player dealer, Player player)
        {
            for (int i = 0; i < 34; i++)
                Console.Write("=");
            Console.WriteLine();

            Console.WriteLine(dealer.Name + ": ");
            PrintRoll(dealer.Dices);

            for (int i = 0; i < 34; i++)
                Console.Write("=");
            Console.WriteLine();

            for (int i = 0; i < 34; i++)
                Console.Write("=");
            Console.WriteLine();

            Console.WriteLine(player.Name + ": ");
            PrintRoll(player.Dices);
            for (int i = 0; i < 34; i++)
                Console.Write("=");
            Console.WriteLine();
            Console.WriteLine();

            int payout = Payout(player.Dices, dealer.Dices, player.Bet);
            player.Money += payout;

            if (payout == 0)
                Console.WriteLine("DRAW");
            else if (payout > 0)
                Console.WriteLine(player.Name + " WINS! " + payout + "(" + payout / player.Bet + "X)" + " Perica");
            else
                Console.WriteLine(player.Name + " LOST! " + System.Math.Abs(payout) + "(" + System.Math.Abs(payout / player.Bet) + "X)" + " Perica");

            Console.WriteLine("Money: " + player.Money + " Perica");

            Console.Write("Press Any Key to Continue...");
            Console.ReadKey();
            Console.Clear();
        }
        static int RollEvaluatePair(int[] dice)
        {
            if (dice[0] == dice[1])
            {
                return dice[2];
            }
            else
            {
                return dice[0];
            }

        }
        static void EvaluateEnding(int money)
        {
            Console.WriteLine("Money: " + money + " Perica");
            Console.WriteLine("================================");
            if (money < 0)
            {
                Console.WriteLine("Ending: Worst");
            }
            else if (money < 100)
            {
                Console.WriteLine("Ending: Bad");
            }
            else if (money < 90000)
            {
                Console.WriteLine("Ending: Meh");
            }
            else if (money < 500000)
            {
                Console.WriteLine("Ending: Good");
            }
            else if (money >= 500000)
            {
                Console.WriteLine("Ending: Best");
            }
            Console.WriteLine("Press Any Key to Continue...");
            Console.ReadKey();
        }
        static void RollUntilValid(Player p, int chanceToPisser)
        {
            int bustTries = 3;
            do
            {
                p.Dices.RollDice(chanceToPisser);
                bustTries--;
                Console.WriteLine("--------------------------------------------");
                PrintRoll(p.Dices);
                if (p.Dices.RollType == RollType.Bust)
                {
                    Console.WriteLine("Tries Left: " + bustTries);
                }
                Console.WriteLine("--------------------------------------------");
                Console.Write("Press Any Key to Continue...");
                Console.ReadKey();
                Console.WriteLine();

            } while (bustTries > 0 && p.Dices.RollType == RollType.Bust);
        }
        static int Payout(Roll player, Roll dealer, int bet)
        {
            if (player.RollType == RollType.Pisser)
            {
                if (dealer.RollType == RollType.Bust || dealer.RollType == RollType.OneTwoThree)
                    return 0;
                else
                    return -bet;
            }
            else if (player.RollType == RollType.Bust)
            {
                if (dealer.RollType == RollType.Bust)
                    return bet;
            }
            else if (dealer.RollType == RollType.OneTwoThree)
            {
                if (player.RollType == RollType.OneTwoThree)
                    return bet;
                else
                    return (bet * 1);
            }
            else if (player.RollType == RollType.OneTwoThree)
            {
                return (bet * -2);
            }
            return bet * DetermineHigherRoll(player, dealer);

        }
        static int DetermineHigherRoll(Roll player, Roll dealer)
        {
            if ((int)player.RollType > (int)dealer.RollType)
            {
                return (int)player.RollType;
            }
            else if ((int)player.RollType < (int)dealer.RollType)
            {
                return -1;
            }
            else
            {
                if (player.RollType == RollType.Pair && dealer.RollType == RollType.Pair)
                {
                    if (RollEvaluatePair(player.Values) > RollEvaluatePair(dealer.Values))
                        return 1;
                    else if (RollEvaluatePair(player.Values) < RollEvaluatePair(dealer.Values))
                        return -1;

                }
                else if (player.RollType == RollType.Triples && dealer.RollType == RollType.Triples)
                {
                    if (player.Values[0] > dealer.Values[0])
                        return 3;
                    else if (player.Values[0] < dealer.Values[0])
                        return -1;

                }
                return 0;
            }

        }
        static bool CanBet(int money, int minimumBet)
        {
            return (money >= minimumBet);
        }
    }
}
