﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnderGroundChinchiro
{
    class Roll
    {
        public Roll()
        {
            Values = new int[3];
            for (int i = 0; i < 3; i++)
            {
                Values[i] = 1;
            }
        }
        public RollType RollType;
        public int[] Values;
        public void RollDice(int chanceToPisser)
        {
            ///this determines if the roll will be pisser
            int Temp = RandomHelper.Range(1, 101);

            if (Temp <= chanceToPisser)
            {
                RollType = RollType.Pisser;
                return;
            }
            ///end if

            for (int i = 0; i < Values.Length; i++)
            {
                Values[i] = RandomHelper.Range(1, 7);
            }
            RollType = RollEvaluator.EvaluateRoll(this);

        }

    }
}
