﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleApplication2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Name of your warrior?");
            string name = Console.ReadLine();
            // int x = Console.Read();//
            Console.WriteLine("Hello " + name);
            string hello = Console.ReadLine();
  

            int hp, ehp;
            string input;
            Console.WriteLine("Determine {0}'s Health..", name);
            string determine = Console.ReadLine();
            hp = Convert.ToInt32(Console.ReadLine());
            Console.Read();

            //health of your nemenemy
            Console.WriteLine("Health of your enemy?");
            string Health = Console.ReadLine();
            ehp = Convert.ToInt32(Console.ReadLine());
            Console.Read();

            //Damges
            int pd, pdmax, ed, edmax;
            Console.WriteLine("Minimum Damage of your hero");
            string Mindmg = Console.ReadLine();
            pd = Convert.ToInt32(Console.ReadLine());
            Console.Read();
            Console.WriteLine("Max Damage?");
            string maxdmg = Console.ReadLine();
            pdmax = Convert.ToInt32(Console.ReadLine());
            Console.Read();
            Console.WriteLine("Minimum Damage of your enemy?");
            string edmg = Console.ReadLine();
            ed = Convert.ToInt32(Console.ReadLine());
            Console.Read();
            Console.WriteLine("Max Damage?");
            string emaxdmg = Console.ReadLine();
            edmax = Convert.ToInt32(Console.ReadLine());
            Console.ReadLine();

            while (hp >= 0 && ehp >= 0)
            {
                Console.Clear();
                Console.WriteLine("Fight!\n");         
                Console.WriteLine("{0}'s HP is {1}", name, hp); // array name
                Console.WriteLine("Enemy: {0}\n", ehp);
                Console.WriteLine("Moves:");
                Console.WriteLine("===============");
                Console.WriteLine("Attack");              
                Console.WriteLine("Defend");             
                Console.WriteLine("Wild Attack");
                input = Console.ReadLine();
                // random
                Random userdamage = new Random();
                int udmg = userdamage.Next(pd, pdmax + 1);
                Random enemydamage = new Random();
                int ai = enemydamage.Next(1, 3 + 1);
                          
                if (input == "attack") 
                {
                    if (ai == 1)
                    {
                        Console.WriteLine("\nYou hurt the enemy.");
                        ehp -= udmg;
                        Console.WriteLine("\nThe enemy received {0} damage", udmg);
                        Console.WriteLine("The enemy attack");
                        Console.WriteLine("\nYou received {0} damage.", enemydamage.Next(ed, edmax + 1));
                        hp -= enemydamage.Next(ed, edmax + 1);
                        Console.ReadLine();
                    }
                    else if (ai == 2)
                    {
                        Console.WriteLine("\nYou scratch the enemy.");
                        ehp -= udmg / 2;
                        Console.WriteLine("\nThe enemy received {0} damage", udmg / 2);
                        Console.WriteLine("The enemy defended");
                        Console.WriteLine("\nYou received 0 damage.");
                        Console.ReadLine();              
                    }
                    else if (ai == 3)
                    {
                        Console.WriteLine("\nYou attack the enemy but he critically damage you");
                        Console.WriteLine("\nThe enemy received {0} damage", udmg);
                        ehp -= udmg;                      
                        Console.WriteLine("\nWild");
                        hp -= enemydamage.Next(ed, edmax + 1) * 2;
                        Console.WriteLine("\nYou received {0} damage.", enemydamage.Next(ed, edmax + 1) * 2);
                        Console.ReadLine();
                    }
                }
                if (input == "defend")
                {
                    if (ai == 1)
                    {
                        Console.WriteLine("You Defended.");
                        Console.WriteLine("\n The enemy scratch you.");
                        hp -= enemydamage.Next(ed, edmax + 1) / 2;
                        Console.WriteLine("\nYou received {0} damage.", enemydamage.Next(ed, edmax + 1) / 2);
                        Console.WriteLine("\nThe enemy received 0 damage");
                        Console.ReadLine();
                    }
                    else if (ai == 2)
                    {
                        Console.WriteLine("\nNothing Happens.");
                        Console.WriteLine("\nThe enemy received 0 damage");
                        Console.WriteLine("\nYou received 0 damage.");
                        Console.ReadLine();
                    }
                    else if (ai == 3)
                    {
                        Console.WriteLine("\nYou Critically damage the enemy.");
                        ehp -= udmg * 2;                   
                        Console.WriteLine("\nThe enemy received {0} damage", udmg *2);
                        Console.WriteLine("\nWIld");
                        Console.WriteLine("\nYou received 0 damage.");
                        Console.ReadLine();
                    }                
                }
                if (input == "wild")
                {
                    if (ai == 1)
                    {
                        Console.WriteLine("\nYou used wild.");
                        ehp -= udmg * 2;
                        Console.WriteLine("\nThe enemy received {0} damage", udmg * 2);
                        Console.WriteLine("\nThe enemy used attack");
                        hp -= enemydamage.Next(ed, edmax + 1);
                        Console.WriteLine("\nYou received {0} damage.", enemydamage.Next(ed, edmax + 1) * 2);
                        Console.ReadLine();
                    }
                    else if (ai == 2)
                    {
                        Console.WriteLine("\nYou miss the attack but he counters you.");
                        Console.WriteLine("\nThe enemy received 0 damage");
                        Console.WriteLine("\nThe enemy used wild");
                        hp -= enemydamage.Next(ed, edmax + 1) * 2;
                        Console.WriteLine("\nYou received {0} damage.", enemydamage.Next(ed, edmax + 1));
                        Console.ReadLine();
                    }
                    else if (ai == 3)
                    {
                        Console.WriteLine("\nYou critically damage the enemy.");
                        ehp -= udmg * 2;                   
                        Console.WriteLine("\nThe enemy received {0} damage", udmg *2);
                        Console.WriteLine("\n The enemy critically damage you.");
                        hp -= enemydamage.Next(ed, edmax + 1) * 2;
                        Console.WriteLine("\nYou received {0} damage.", enemydamage.Next(ed, edmax + 1) * 2);
                        Console.ReadLine();
                    }
                }            
            }
            if (hp < 0)
            {
                Console.WriteLine("\nYour HP is now 0. ");
                Console.WriteLine("\n GAME OVER!");
                Console.ReadKey();
            }
            else if (ehp < 0)
            {
                Console.WriteLine("Congratulations! You have killed the enemy!");
                Console.ReadKey();
            }
        }
    }
}


