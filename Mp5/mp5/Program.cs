﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mp5
{
    class Program
    {
        static void Main(string[] args)
        {
            Unit player = new Unit();
            Unit monster = new Unit();
            player.player();
            player.print();
            int level = 1;

            //RandomMonster
            while (player.hp >= 0)
            {
                Console.WriteLine(" ");
                Console.WriteLine(" Level {0} \n", level);
                Console.WriteLine(" VS ");
                monster.enemy(level);
                monster.print();
                Console.WriteLine("");
                while (player.hp >= 0 && monster.hp >= 0)
                {
                    if (player.agi >= monster.agi)
                    {
                        Console.ReadLine();
                        Console.Clear();
                        Console.WriteLine("===BATTLE===");
                        player.battle(monster);
                        monster.battle(player);
                    }
                    else if (monster.agi >= player.agi)
                    {
                        Console.ReadLine();
                        Console.Clear();
                        Console.WriteLine("===BATTLE===");
                        monster.battle(player);
                        player.battle(monster);
                    }
                    if (monster.hp <= 0)
                    {
                        player.statBonus(monster);
                        break;
                    }
                    else if (player.hp <= 0)
                    {
                        Console.WriteLine("GAME OVER!");
                        break;                   
                    }
                }
                if (player.hp <= 0)
                {
                    Console.WriteLine("PLAYER REACHED Level {0}", level);
                    break;
                }
                level++;
            }
            Console.ReadLine();
        }
    }
}
