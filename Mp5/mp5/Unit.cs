﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mp5
{
    class Unit
    {
        public string name;
        public int hp;
        public int pow;
        public int vit;
        public float agi;
        public float dex;
        public float bdmg;
        public int damage;
        public int random;
        public float hit;
        public void battle(Unit defender)
        {
            Random rnd = new Random();
            //bonus damage

            if ((this.name == "Assassin" || this.name == "assassin") && (defender.name == "Mage" || defender.name == "mage"))
            {
                this.bdmg = 1.5f;
            }
            else if ((this.name == "Assassin" || this.name == "assassin") && (defender.name == "Warrior" || defender.name == "warrior"))
            {
                this.bdmg = 1.5f;
            }
            else if ((this.name == "Warrior" || this.name =="warrior") && (defender.name == "Assassin" || defender.name == "assassin"))
            {
                this.bdmg = 1.5f;
            }
            else if ((this.name == "Warrior" || this.name == "warrior") && (defender.name == "Mage" || defender.name == "mage"))
            {
                this.bdmg = 1.5f;
            }
            else if ((this.name == "Mage" || this.name == "mage") && (defender.name == "Warrior" || defender.name == "warrior"))
            {
                this.bdmg = 1.5f;
            }
            else if ((this.name == "Mage" || this.name == "mage") && (defender.name == "Assassin" || defender.name == "assassin"))
            {
                this.bdmg = 1.5f;
            }
            else
            {
                this.bdmg = 1.0f;
            }
            this.hit = (this.dex / defender.agi) * 100;
            //hitrate
            if (this.hit <= 20)
            {
                this.hit = 20;
            }
            else if(this.hit >= 80)
            {
                this.hit = 80;
            }
            random = rnd.Next(1, 101);
            //deals damage
            if (this.hp > 0 && defender.hp > 0)
            {
                if (random <= this.hit)
                {
                    this.damage = (int)((this.pow - defender.vit) * this.bdmg);
                    //clamps damage to 1 
                    if (this.damage <= 0) this.damage = 1;
                    //Console.WriteLine("Bonus Damage: " + this.damage);
                    Console.WriteLine("Damage: " + this.damage);

                    //decreases hp
                    defender.hp -= this.damage;
                    if (defender.hp <= 0)
                    {
                        defender.hp = 0;
                    }
                        Console.WriteLine(this.name + " Attacks " + defender.name);
                        Console.WriteLine("HP deducted by " + this.damage);
                        Console.WriteLine(defender.name + " HP: " + defender.hp);
                    
                }
                else if (random >= this.hit)
                {
                    Console.WriteLine(this.name + " missed " + defender.name);
                }
            }
            Console.ReadLine();
        }
        public void print()
        {
            Console.WriteLine("name: " + name);
            Console.WriteLine("HP: " + hp);
            Console.WriteLine("Pow:" + pow);
            Console.WriteLine("Vit:" + vit);
            Console.WriteLine("Agi:" + agi);
            Console.WriteLine("Dex:" + dex);
        }

        //PLAYER CLASS
        public void player()
        {
            Random rnd = new Random();
            Console.WriteLine("Select your Class");
            Console.WriteLine("Class\n");
            Console.WriteLine("Assassin");
            Console.WriteLine("Warrior");
            Console.WriteLine("Mage");

            name = Convert.ToString(Console.ReadLine());
            if (name == "assassin" || name == "Assassin")
            {
                Console.WriteLine("You have selected {0}", name);
                this.hp = rnd.Next(5, 11);
                this.pow = rnd.Next(3, 6);
                this.vit = rnd.Next(1,4);
                this.agi = rnd.Next(5, 11);
                this.dex = rnd.Next(1,5);            
            }
             else if (name == "warrior" || name == "Warrior")
            {
                Console.WriteLine("You have selected {0}", name);
                this.hp = rnd.Next(5, 16);
                this.pow = rnd.Next(8, 11);
                this.vit = rnd.Next(6, 8);
                this.agi = rnd.Next(1, 6);
                this.dex = rnd.Next(1, 5);
            }
           else  if (name == "mage" || name == "Mage")
            {
                Console.WriteLine("You have selected {0}", name);
                this.hp = rnd.Next(5, 11);
                this.pow = rnd.Next(9, 12);
                this.vit = rnd.Next(5, 7);
                this.agi = rnd.Next(1, 6);
                this.dex = rnd.Next(1, 5);
            }
            Console.ReadLine();
        }

        //ENEMY CLASS
        public void enemy(int level)
        {
            Random rnd = new Random();           
            random = rnd.Next(1,4);
            name = Convert.ToString(Console.ReadLine());
            if (random== 1)
            {
                Console.WriteLine("Enemy have selected Assassin");
                this.name = "Assassin";
                this.hp = rnd.Next(1,6)* level;
                this.pow = rnd.Next(1, 6) * level;
                this.vit = rnd.Next(1, 4) * level;
                this.agi = rnd.Next(5, 11) * level;
                this.dex = rnd.Next(5, 11) * level;
            }
            else if (random == 2)
            {
                Console.WriteLine("Enemy have selected Warrior");
                this.name = "Warrior";
                this.hp = rnd.Next(1, 6) * level;
                this.pow = rnd.Next(1,6) * level;
                this.vit = rnd.Next(6, 8) * level;
                this.agi = rnd.Next(1, 6) * level;
                this.dex = rnd.Next(1, 7) * level;
            }
            else if (random == 3)
            {
                Console.WriteLine("Enemy have selected Mage");
                this.name = "Mage";
                this.hp = rnd.Next(1, 6) * level;
                this.pow = rnd.Next(1, 6) * level;
                this.vit = rnd.Next(5, 11) * level;
                this.agi = rnd.Next(1, 6) * level;
                this.dex = rnd.Next(5, 11) * level;
            }
            Console.ReadLine();
        }

        // STATBONUS
        public void statBonus(Unit enemy)
        {
            if ((this.name == "Assassin" || this.name == "assassin") && enemy.name == "Warrior")
            {
                Console.WriteLine("You are healed by {0}", (int)(this.hp * 0.3f));
                Console.WriteLine("Stats added ");
                Console.WriteLine("HP upgraded by {0}", this.hp += 3);
                Console.WriteLine("Vit upgraded by {0}", this.vit += 3);
            }

            else if ((this.name == "Assassin" || this.name == "assassin") && enemy.name == "Assassin")
            {
                Console.WriteLine("You are healed by {0}", (int)(this.hp * 0.3f));
                Console.WriteLine("Stats added ");
                Console.WriteLine("HP upgraded by {0}", this.hp += 3);
                Console.WriteLine("Dex upgraded by {0}", this.dex += 3);             
            }
            else if((this.name == "Assassin" || this.name == "assassin") && enemy.name == "Mage")
            {
                Console.WriteLine("You are healed by {0}", (int)(this.hp * 0.3f));
                Console.WriteLine("Stats added ");
                Console.WriteLine("power upgraded by 5");
                Console.WriteLine("Total power {0}", this.pow += 5);
            }
            if ((this.name == "Warrior" || this.name == "warrior") && enemy.name == "Warrior")
            {
                Console.WriteLine("You are healed by {0}", (int)(this.hp * 0.3f));
                Console.WriteLine("Stats added ");
                Console.WriteLine("HP upgraded by {0}", this.hp += 3);
                Console.WriteLine("Vit upgraded by {0}", this.vit += 3);
            }

            else if ((this.name == "Warrior" || this.name == "warrior") && enemy.name == "Assassin")
            {
                Console.WriteLine("You are healed by {0}", (int)(this.hp * 0.3f));
                Console.WriteLine("Stats added ");
                Console.WriteLine("HP upgraded by {0}", this.hp += 3);
                Console.WriteLine("Dex upgraded by {0}", this.dex += 3);
            }
            else if ((this.name == "Warrior" || this.name == "warrior") && enemy.name == "Mage")
            {
                Console.WriteLine("You are healed by {0}", (int)(this.hp * 0.3f));
                Console.WriteLine("Stats added ");
                Console.WriteLine("power upgraded by 5");
                Console.WriteLine("Total power {0}", this.pow += 5);
            }
            if ((this.name == "Mage" || this.name == "mage") && enemy.name == "Warrior")
            {
                Console.WriteLine("You are healed by {0}", (int)(this.hp * 0.3f));
                Console.WriteLine("Stats added ");
                Console.WriteLine("HP upgraded by {0}", this.hp += 3);
                Console.WriteLine("Vit upgraded by {0}", this.vit += 3);
            }

            else if ((this.name == "Mage" || this.name == "mage") && enemy.name == "Assassin")
            {
                Console.WriteLine("You are healed by {0}", (int)(this.hp * 0.3f));
                Console.WriteLine("Stats added ");
                Console.WriteLine("HP upgraded by {0}", this.hp += 3);
                Console.WriteLine("Dex upgraded by {0}", this.dex += 3);
            }
            else if ((this.name == "Mage" || this.name == "mage") && enemy.name == "Mage")
            {
                Console.WriteLine("You are healed by {0}", (int)(this.hp * 0.3f));
                Console.WriteLine("Stats added ");
                Console.WriteLine("power upgraded by 5");
                Console.WriteLine("Total power {0}", this.pow += 5);
            }
            Console.ReadLine();
        }
    }
    


}
